#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define STUDENTS_COUNT 10

class Student {
	private:
		string studentNo;
		string studentName;
		string studentSurname;
		int studentStatus;
		
	public:		
		Student(string studentNo,string studentName,string studentSurname,int studentStatus) {
			this->studentNo = studentNo;
			this->studentName = studentName;
			this->studentSurname = studentSurname;
			this->studentStatus = studentStatus;
		}
		
		string getStudentNo() {
			return this->studentNo;
		}
		
		string getStudentName() {
			return this->studentName;
		}
		

		string getStudentSurname() {
			return this->studentSurname;
		}
		
		int getStudentStatus(){
			return this->studentStatus;
		}
		
		
};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	
	ss << randomNumber;
	
	return ss.str();
}

string getRandomStudentName(){
	string list[3]={"Mato","Andre","Bodzio"};
	return list[rand() % 3];
}

string getRandomStudentSurname(){
	string list[3]={"Katol","Kowalski","Zielonka"};
	return list[rand() % 3];
}

int getRandomStudnetStatus(){	
	return rand() % 2;
}


int main() {
	vector<Student> students;
	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student(getRandomStudentNumber(),getRandomStudentName(),getRandomStudentSurname(),getRandomStudnetStatus());
	
		students.push_back(student);
	}
	
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		cout << students.at(i).getStudentNo() << " " <<  students.at(i).getStudentName()<< " " <<  students.at(i).getStudentSurname()<< " " <<  students.at(i).getStudentStatus() << endl;
	}
	
	
	
	return 0;
}

